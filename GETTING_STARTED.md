# Getting Started
This is a guide to quickly get up and running with Electron and React!

## Preliminaries
First you need to have the following installed on your computer:

- [Git](https://git-scm.com)
- [Node](https://nodejs.org/en/download/) (which comes with [npm](http://npmjs.com))

Check that Git and Node are installed via a CLI
```bash
# Check for Git
git --version
# Check for Node
node --version
```

## Create an Electron App
To get started, simply clone the [Electron Quick Start](https://github.com/electron/electron-quick-start).
Feel free to rename the project directory whatever you'd like.

```bash
# Clone the electron-quick-start
git clone https://github.com/electron/electron-quick-start
# Go into the repository
cd electron-quick-start
# Install dependencies
npm install
# Run the app
npm start
```

After running the *npm start* command, the Electron app should start and a browser-like window will open up on your screen!

## Add React to the Project
The easiest way to get React up and running inside of your Electron app is via the [electron-prebuilt-compile package](https://www.npmjs.com/package/electron-prebuilt-compile). Essentially, this package allows Electron to natively understand React.

```bash
# Install react itself
npm install --save react react-dom
# Install electron-prebuilt-compile as a development dependency
npm install electron-prebuilt-compile --save-dev
```

## Add some code that uses React
In your project, edit the <body> of your *index.html* so that it looks like:
```html
<body>
  <div id="root"></div>
  <script>
    // You can also require other files to run in this process
    require('./renderer.js')
  </script>
</body>
```

Now, add the following to your *renderer.js*:
```javascript
import React from 'react';
import ReactDOM from 'react-dom';

ReactDOM.render(
  <h1> Hello World </h1>,
  document.getElementById('root')
);
```

## Start the App
Your app is all ready to go!

```bash
# Run the app again
npm start
```

The Electron app should launch the same as before, but now there should be a nice big header displaying "Hello World"

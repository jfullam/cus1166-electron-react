#Lab Exercise

1. Go through the [Getting Started Guide](GETTING_STARTED.md) to get a simple Electron App up and running with React
2. Clone this repo and get the [Connect Four Demo](electron-react-connect-four) up and running
3. Modify renderer.js to add a button that restarts the game when clicked.

# Home of John's Introduction to Electron and React

To do anything with this repo, you'll need [Git](https://git-scm.com) and [Node.js](https://nodejs.org/en/download/) (which comes with [npm](http://npmjs.com)) installed on your computer. From your command line:

```bash
# Clone this repository
git clone https://bitbucket.org/jfullam/cus1166-electron-react
# Go into the repository
cd cus1166-electron-react
```

## Quick Links

- [Getting Started Guide](GETTING_STARTED.md)
- [Connect Four Demo](electron-react-connect-four)
- [Lab Exercise](LAB_EXERCISE.md)
- [Electron React Presentation](https://docs.google.com/presentation/d/1VoxiNsTnC80_KiIxOJz2pVBAThRE_1tKVEraAG-JyAM/edit?usp=sharing)

## External References

- [Electron Quick Start Guide](http://electron.atom.io/docs/tutorial/quick-start)
- [Electron API Demos](https://github.com/electron/electron-api-demos)
- [React Tutorial: Intro To React](https://reactjs.org/tutorial/tutorial.html)
- [electron-prebuilt-compile](https://www.npmjs.com/package/electron-prebuilt-compile)

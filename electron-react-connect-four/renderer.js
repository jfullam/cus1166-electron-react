// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.
import React from 'react';
import ReactDOM from 'react-dom';

class Game extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      current_player: "Player 1",
      winner: null,
      squares: new Array(42).fill(null)
    }
  }

  render() {
    return (
      <div className="game">
        <h1> Connect Four (Electron + React)</h1>
        <Board
          squares={this.state.squares}
          onClick={(i) => this.handleClick(i)}/>
        <h2>
          {this.getGameInfo()}
        </h2>
      </div>
    )
  }

  getGameInfo() {
    if (this.state.winner == null) {
      return this.state.current_player + "'s Turn!";
    }
    else {
      return this.state.winner + " Wins!!!";
    }
  }

  handleClick(i) {
    if (this.state.winner != null) {
      return;
    }

    const squares = this.state.squares.slice();
    if(i<35) {
      if(squares[i+7]==null) {
        return;
      }
    }

    let marker = "X";
    if (this.state.current_player == "Player 2") {
      marker = "O";
    }
    squares[i]=marker;

    let winner = this.checkForWinner(squares);
    if (winner!=null && winner=="X") {
      winner = "Player 1";
    }
    else if (winner!=null) {
      winner = "Player 2";
    }

    let next_player = this.state.current_player;
    if (winner==null) {
      next_player = this.getNextPlayer();
    }

    //This is it what causes the app to refresh and update its display
    this.setState({
      squares: squares,
      winner: winner,
      current_player: next_player
    });
  }

  checkForWinner(squares) {
    for (let i=0; i<42; i++) {
      if(squares[i]!=null) {
        if (this.checkForHorizontalWinner(squares,i,1)) {return squares[i];}
        if (this.checkForVerticalWinner(squares,i,1)) {return squares[i];}
        if (this.checkForAscendingDiagonalWinner(squares,i,1)) {return squares[i];}
        if (this.checkForDescendingDiagonalWinner(squares,i,1)) {return squares[i];}
      }
    }
    return null;
  }

  checkForHorizontalWinner(squares,i,n) {
    if (n==4) {
      return true;
    }
    let j=i+1;
    if (j>=42 || (i%7 > j%7) || squares[i]!=squares[j]) { return false; }
    else { return (this.checkForHorizontalWinner(squares,j,n+1)); }
    return false;
  }

  checkForVerticalWinner(squares,i,n) {
    if (n==4) {
      return true;
    }
    let j=i+7;
    if (j>=42 || squares[i]!=squares[j]) { return false; }
    else { return (this.checkForVerticalWinner(squares,j,n+1)); }
    return false;
  }

  checkForAscendingDiagonalWinner(squares,i,n) {
    if (n==4) {
      return true;
    }
    let j=i+8;
    if (j>=42 || (i%7 > j%7) || squares[i]!=squares[j]) { return false; }
    else { return (this.checkForAscendingDiagonalWinner(squares,j,n+1)); }
    return false;
  }

  checkForDescendingDiagonalWinner(squares,i,n) {
    if (n==4) {
      return true;
    }
    let j=i+6;
    if (j>=42 || (i%7 < j%7) || squares[i]!=squares[j]) { return false; }
    else { return (this.checkForDescendingDiagonalWinner(squares,j,n+1)); }
    return false;
  }

  getNextPlayer() {
    if (this.state.current_player == "Player 1") {
      return "Player 2";
    }
    else {
      return "Player 1";
    }
  }
}

class Board extends React.Component {
  render() {
    return(
      <div className="game-board">
        {this.props.squares.map((square, index) => this.renderSquare(square, index))}
      </div>
    );
  }

  renderSquare(square, i) {
    return (
      <Square
        key={i}
        value={square}
        className={this.getClassName(i)}
        onClick={() => this.props.onClick(i)}/>
    );
  }

  getClassName(i) {
    if ((this.props.squares[i]==null) && (i>=35 || (i<35 && this.props.squares[i+7]!=null))) {
      return "square clickable";
    }
    return "square";
  }
}

function Square(props) {
  return (
    <button className={props.className} onClick={props.onClick}>
      {props.value}
    </button>
  );
}

ReactDOM.render(
  <Game/>,
  document.getElementById('root')
);

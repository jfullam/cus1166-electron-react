# electron-react-connect-four
This simple game of Connect Four is built with Electron and React!

The app uses the electron-prebuilt-compile package, which allows Electron to natively understand React.
In my experience, this is the easiest way for a beginner to get the two technologies up and running together.

## To Use
To run the app:

```bash
cd electron-react-connect-four
# Install dependencies
npm install
# Run the app
npm start
```

## License
[CC0 1.0 (Public Domain)](LICENSE.md)
